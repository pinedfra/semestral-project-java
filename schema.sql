CREATE DATABASE university_db;

CREATE TABLE students
(
    id           VARCHAR(150) NOT NULL,
    first_name   VARCHAR(45)  NOT NULL,
    last_name    VARCHAR(45)  NOT NULL,
    faculty_name VARCHAR(60)  NOT NULL,
    address      VARCHAR(45),
    email        VARCHAR(45),
    number_year  SMALLINT     NOT NULL,
    CONSTRAINT pk_students PRIMARY KEY (id)
);
CREATE TABLE professors
(
    id           VARCHAR(150) NOT NULL,
    first_name   VARCHAR(45)  NOT NULL,
    last_name    VARCHAR(45)  NOT NULL,
    faculty_name VARCHAR(60)  NOT NULL,
    address      VARCHAR(45),
    email        VARCHAR(45),
    cellphone    VARCHAR(25),
    CONSTRAINT pk_professors PRIMARY KEY (id)
);

CREATE TABLE subjects
(
    id      BIGINT NOT NULL GENERATED ALWAYS AS IDENTITY,
    name    VARCHAR(45) UNIQUE NOT NULL,
    credits INT NOT NULL DEFAULT 0,
    CONSTRAINT pk_subjects PRIMARY KEY (id)
);
CREATE TABLE students_take_subjects
(
    student_id VARCHAR(255) NOT NULL,
    subject_id BIGINT       NOT NULL,
    CONSTRAINT pk_students_take_subjects PRIMARY KEY (student_id, subject_id)
);

ALTER TABLE students_take_subjects
    ADD CONSTRAINT FK_STUDENTS_TAKE_SUBJECTS_ON_STUDENT FOREIGN KEY (student_id) REFERENCES students (id) ON DELETE CASCADE;

ALTER TABLE students_take_subjects
    ADD CONSTRAINT FK_STUDENTS_TAKE_SUBJECTS_ON_SUBJECT FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE;
CREATE TABLE professors_teach_subjects
(
    professor_id VARCHAR(255) NOT NULL,
    subject_id   BIGINT       NOT NULL,
    CONSTRAINT pk_professors_teach_subjects PRIMARY KEY (professor_id, subject_id)
);

ALTER TABLE professors_teach_subjects
    ADD CONSTRAINT FK_PROFESSORS_TEACH_SUBJECTS_ON_PROFESSOR FOREIGN KEY (professor_id) REFERENCES professors (id) ON DELETE CASCADE;

ALTER TABLE professors_teach_subjects
    ADD CONSTRAINT FK_PROFESSORS_TEACH_SUBJECTS_ON_SUBJECT FOREIGN KEY (subject_id) REFERENCES subjects (id) ON DELETE CASCADE;