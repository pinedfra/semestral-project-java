# semestral-project-java
---
This project is an API about students choosing subjects

## Development status
---
This project is completed

## Instructions
---
This Project is made in Java 18 and PostgresSQL, so it's necessary to have both installed in your computer.

### Installation

1. Clone this repository
2. Open the project in your IDE
3. Do the PostgresSQL configuration below
4. Run the project
5. Open your browser and go to http://localhost:8080/university/api/swagger-ui/index.html
6. You can see the API documentation

### PostgresSQL
---
In order to have the same schema for database, you can create the database from the file `schema.sql` in the root of the project.
It's necessary to change the username and password in the file `application.yml` in src/main/resources in order to connect spring boot with the database.

### Client
---
You can find the client for this API in the following repository:
https://gitlab.fit.cvut.cz/pinedfra/semestral-project-java-client

### Entity–relationship model

Students can take subjects and the subjects are taught by the professors

[![database-model.jpg](https://i.postimg.cc/ZRsvysRG/database-model.jpg)](https://postimg.cc/8JWz2HL4)

***
### Complex query
Number of students who are in the course of Java Technology and they are in their 3rd year or more
``` spel
@Query("SELECT s FROM Student s" +
            " JOIN s.subjects sub" +
            " WHERE sub.name = :name and s.numberYear >= :numberYear")
    Collection<Student> findStudentsBySubjectNameAndNumberYear(String name, Character numberYear);  
```

***
### Complex business logic operation
There must be a method which doesn't allow students to take more than 30 credits
