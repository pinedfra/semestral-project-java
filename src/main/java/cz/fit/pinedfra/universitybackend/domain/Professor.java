package cz.fit.pinedfra.universitybackend.domain;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "professors")
public class Professor extends AbstractEntity {
    @Column(length = 25)
    private String cellphone;

    @ManyToMany
    @JoinTable(
            name = "professors_teach_subjects",
            joinColumns = @JoinColumn(name = "professor_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id")
    )
    private List<Subject> subjects = new ArrayList<>();

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
        subject.getProfessors().add(this);
    }

    public void removeSubject(long subjectId){
        Subject subject = this.subjects.stream().filter(s -> s.getId() == subjectId).findFirst().orElse(null);
        if(subject != null){
            this.subjects.remove(subject);
            subject.getProfessors().remove(this);
        }
    }
}
