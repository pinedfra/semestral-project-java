package cz.fit.pinedfra.universitybackend.domain;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Getter
@Setter
@MappedSuperclass
public abstract class AbstractEntity implements Serializable, DomainEntity<String> {

    @Id
    @Column(length = 150)
    @NotBlank
    private String id;

    @Column(name = "first_name", length = 45, nullable = false)
    private String firstName;

    @Column(name = "last_name", length = 45, nullable = false)
    private String lastName;

    @Column(name = "faculty_name", length = 60, nullable = false)
    private String facultyName;

    @Column(length = 45)
    private String address;

    @Column(length = 45)
    private String email;
}
