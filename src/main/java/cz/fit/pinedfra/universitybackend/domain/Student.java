package cz.fit.pinedfra.universitybackend.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "students")
public class Student extends AbstractEntity {
    @Column(name = "number_year", columnDefinition = "smallint", length = 1, nullable = false)
    private Integer numberYear;

    @ManyToMany
    @JoinTable(
            name = "students_take_subjects",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "subject_id", referencedColumnName = "id")
    )
    private List<Subject> subjects = new ArrayList<>();

    public void addSubject(Subject subject) {
        this.subjects.add(subject);
        subject.getStudents().add(this);
    }

    public void removeSubject(long subjectId){
        Subject subject = this.subjects.stream().filter(s -> s.getId() == subjectId).findFirst().orElse(null);
        if(subject != null){
            this.subjects.remove(subject);
            subject.getStudents().remove(this);
        }
    }
}
