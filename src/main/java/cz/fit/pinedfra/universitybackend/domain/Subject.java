package cz.fit.pinedfra.universitybackend.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "subjects")
public class Subject implements DomainEntity<Long>{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 45, nullable = false, unique = true)
    private String name;

    @Column(nullable = false)
    private Integer credits;

    @ManyToMany(mappedBy = "subjects")
    @JsonIgnore
    private List<Student> students;

    @ManyToMany(mappedBy = "subjects")
    @JsonIgnore
    private List<Professor> professors = new ArrayList<>();
}
