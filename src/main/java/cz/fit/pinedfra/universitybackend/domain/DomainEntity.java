package cz.fit.pinedfra.universitybackend.domain;

public interface DomainEntity<ID> {

    ID getId();

    void setId(ID id);
}
