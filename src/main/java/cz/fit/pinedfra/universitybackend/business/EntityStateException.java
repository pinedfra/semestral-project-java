package cz.fit.pinedfra.universitybackend.business;

public class EntityStateException extends RuntimeException {

    public <E> EntityStateException(E entity) {
        super((String) entity);
    }
}
