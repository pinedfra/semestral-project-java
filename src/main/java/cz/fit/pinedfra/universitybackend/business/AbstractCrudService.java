package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.domain.DomainEntity;
import org.springframework.dao.DataAccessException;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityExistsException;
import java.util.Optional;

public abstract class AbstractCrudService<E extends DomainEntity<K>, K> {

    protected final CrudRepository<E, K> repository;

    protected AbstractCrudService(CrudRepository<E, K> repository) {
        this.repository = repository;
    }

    public E create(E entity) {
        try {
            if ("".equals(entity.getId()) || repository.existsById(entity.getId())) {
                throw new EntityExistsException("Invalid input");
            }
            return repository.save(entity);
        } catch (EntityStateException e) {
            throw new EntityStateException(e.getMessage());
        } catch (Exception e) {
            throw new EntityStateException("Invalid input");
        }
    }

    public Optional<E> readById(K id) {
        return repository.findById(id);
    }

    public Iterable<E> readAll() {
        return repository.findAll();
    }

    public E update(E entity) {
        try {
            if (!repository.existsById(entity.getId()) || "".equals(entity.getId())) {
                throw new EntityStateException("Invalid input");
            }
            return repository.save(entity);
        } catch (Exception e) {
            throw new EntityStateException("Invalid input");
        }
    }

    @Transactional
    public boolean deleteById(K id) throws DataAccessException {
        if (repository.existsById(id)) {
            repository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }
}
