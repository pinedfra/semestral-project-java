package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.dao.ProfessorRepository;
import cz.fit.pinedfra.universitybackend.dao.SubjectRepository;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@Service
public class ProfessorService extends AbstractCrudService<Professor, String> {

    private final SubjectRepository subjectRepository;

    public ProfessorService(ProfessorRepository repository, SubjectRepository subjectRepository) {
        super(repository);
        this.subjectRepository = subjectRepository;
    }

    public Collection<Professor> findProfessorBySubjectsId(Long id) {
        try {
            if (subjectRepository.existsById(id)) {
                return ((ProfessorRepository) repository).findProfessorBySubjectsId(id);
            } else {
                throw new EntityStateException("Subject with id " + id + " not found");
            }
        } catch (EntityStateException e) {
            throw new EntityStateException(e.getMessage());
        }
    }

    public String addSubject(String professorId, Long subjectId) {
        try {
            Professor professor = repository.findById(professorId).orElse(null);
            Subject subject = subjectRepository.findById(subjectId).orElse(null);
            if (professor == null || subject == null ) {
                throw new EntityStateException("Professor or subject not found");
            }
            boolean enrolled = professor.getSubjects().stream().anyMatch(s -> s.getId().equals(subjectId));
            if (enrolled) {
                return "Subject already enrolled";
            }
            professor.addSubject(subject);
            repository.save(professor);
            return "Subject added";
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }

    public String removeSubject(String professorId, Long subjectId) {
        try {
            Professor professor = repository.findById(professorId).orElse(null);
            Subject subject = subjectRepository.findById(subjectId).orElse(null);
            if (professor == null || subject == null ) {
                throw new EntityStateException("Professor or subject not found");
            }
            boolean teaching = professor.getSubjects().stream().anyMatch(s -> s.getId().equals(subjectId));
            if (!teaching) {
                return "The professor is not teaching this subject";
            }
            professor.removeSubject(subjectId);
            repository.save(professor);
            return "Subject removed";
        } catch (EntityNotFoundException e) {
            throw new EntityNotFoundException(e.getMessage());
        }
    }
}
