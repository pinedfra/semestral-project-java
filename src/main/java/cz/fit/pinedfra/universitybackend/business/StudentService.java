package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.dao.StudentRepository;
import cz.fit.pinedfra.universitybackend.dao.SubjectRepository;
import cz.fit.pinedfra.universitybackend.domain.Student;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Collection;

@Service
public class StudentService extends AbstractCrudService<Student, String> {

    private final SubjectRepository subjectRepository;

    public StudentService(StudentRepository repository, SubjectRepository subjectRepository) {
        super(repository);
        this.subjectRepository = subjectRepository;
    }

    public Collection<Student> findStudentsBySubjectId(Long id) {
        try {
            if (subjectRepository.existsById(id)) {
                return ((StudentRepository) repository).findStudentBySubjectsId(id);
            } else {
                throw new EntityStateException("Subject not found");
            }
        } catch (EntityStateException e) {
            throw new EntityStateException("Subject with id " + id + " not found");
        }
    }
    public Collection<Student> findStudentBySubjectsNameAndNumberYear(String subjectName, Integer numberYear) {
        try {
            Collection<Student> students = ((StudentRepository) repository).findStudentBySubjectsNameAndNumberYear(subjectName, numberYear);
            if (students.isEmpty()) {
                throw new EntityStateException("No students found");
            } else {
                return students;
            }
        } catch (EntityStateException e) {
            throw new EntityStateException(e.getMessage());
        }
    }

    public String addSubject(String studentId, Long subjectId) {
        try {
            Student student = repository.findById(studentId).orElse(null);
            Subject subject = subjectRepository.findById(subjectId).orElse(null);
            if (student == null || subject == null ) {
                throw new EntityStateException("Student or subject not found");
            }
            boolean enrolled = student.getSubjects().stream().anyMatch(s -> s.getId().equals(subjectId));
            if (enrolled) {
                return "Subject already enrolled";
            }
            student.addSubject(subject);
            repository.save(student);
            return "Subject added";
        } catch (EntityNotFoundException e) {
            return e.getMessage();
        }
    }

    public String removeSubject(String studentId, Long subjectId) {
        try {
            Student student = repository.findById(studentId).orElse(null);
            Subject subject = subjectRepository.findById(subjectId).orElse(null);
            if (student == null || subject == null ) {
                throw new EntityStateException("Student or subject not found");
            }
            boolean studying = student.getSubjects().stream().anyMatch(s -> s.getId().equals(subjectId));
            if (!studying) {
                return "Student is not studying this subject";
            }
            student.removeSubject(subjectId);
            repository.save(student);
            return "Subject removed";
        } catch (EntityNotFoundException e) {
            return e.getMessage();
        }
    }
}
