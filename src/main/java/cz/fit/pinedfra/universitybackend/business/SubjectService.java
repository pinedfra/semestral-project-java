package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.dao.ProfessorRepository;
import cz.fit.pinedfra.universitybackend.dao.StudentRepository;
import cz.fit.pinedfra.universitybackend.dao.SubjectRepository;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SubjectService extends AbstractCrudService<Subject, Long> {

    @Autowired
    protected ProfessorRepository professorRepository;

    @Autowired
    protected StudentRepository studentRepository;

    public SubjectService(SubjectRepository repository) {
        super(repository);
    }

    public Collection<Subject> findSubjectsByStudentId(String id) {
        if (!studentRepository.existsById(id)) {
            throw new EntityStateException("Student with id " + id + " does not exist");
        }
        return ((SubjectRepository) repository).findSubjectByStudentsId(id);
    }

    public Collection<Subject> findSubjectsByProfessorId(String id) {
        if (!professorRepository.existsById(id)) {
            throw new EntityStateException("Professor with id " + id + " does not exist");
        }
        return ((SubjectRepository) repository).findSubjectByProfessorsId(id);
    }
}
