package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.domain.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface StudentRepository extends JpaRepository<Student, String> {

    @Query("SELECT s FROM Student s" +
            " JOIN s.subjects sub" +
            " WHERE sub.name = :name and s.numberYear >= :numberYear")
    Collection<Student> findStudentBySubjectsNameAndNumberYear(@Param("name") String name,@Param("numberYear") Integer numberYear);

    Collection<Student> findStudentBySubjectsId(Long id);
}