package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface SubjectRepository extends JpaRepository<Subject, Long> {

    Collection<Subject> findSubjectByStudentsId (String id);
    Collection<Subject> findSubjectByProfessorsId (String id);
}