package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.domain.Professor;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProfessorRepository extends JpaRepository<Professor, String> {

    Collection<Professor> findProfessorBySubjectsId (Long id);
}