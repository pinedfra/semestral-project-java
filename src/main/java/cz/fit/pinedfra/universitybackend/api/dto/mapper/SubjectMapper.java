package cz.fit.pinedfra.universitybackend.api.dto.mapper;

import cz.fit.pinedfra.universitybackend.api.dto.SubjectDto;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SubjectMapper extends CrudMapper<Subject, SubjectDto> {
}
