package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.api.dto.SubjectDto;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.SubjectMapper;
import cz.fit.pinedfra.universitybackend.business.SubjectService;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping("/subject")
@CrossOrigin
public class SubjectController extends AbstractCrudController<Subject, SubjectDto, Long> {

    public SubjectController(SubjectService service, SubjectMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/student/{id}")
    public ResponseEntity<Object> findSubjectsByStudentId(@PathVariable String id) {
        try {
            Collection<Subject> subjects = ((SubjectService) service).findSubjectsByStudentId(id);
            return ResponseEntity.ok(mapper.toDtos(subjects));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @GetMapping("/professor/{id}")
    public ResponseEntity<Object> findSubjectsByProfessorId(@PathVariable String id) {
        try {
            Collection<Subject> subjects = ((SubjectService) service).findSubjectsByProfessorId(id);
            return ResponseEntity.ok(mapper.toDtos(subjects));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
