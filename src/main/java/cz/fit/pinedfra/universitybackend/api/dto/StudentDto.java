package cz.fit.pinedfra.universitybackend.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * A DTO for the {@link cz.fit.pinedfra.universitybackend.domain.Student} entity
 */
@Getter
@Setter
public class StudentDto implements Serializable {
    private String id;
    private String firstName;
    private String lastName;
    private String facultyName;
    private String address = "";
    private String email = "";
    private int numberYear = 1;
}