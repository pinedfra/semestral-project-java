package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.api.dto.StudentDto;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.StudentMapper;
import cz.fit.pinedfra.universitybackend.business.EntityStateException;
import cz.fit.pinedfra.universitybackend.business.StudentService;
import cz.fit.pinedfra.universitybackend.domain.Student;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/student")
@CrossOrigin
public class StudentController extends AbstractCrudController<Student, StudentDto, String> {

    public StudentController(StudentService service, StudentMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/subject/{id}")
    public ResponseEntity<Object> findStudentsBySubjectId(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(mapper.toDtos(((StudentService) service).findStudentsBySubjectId(id)));
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
    @GetMapping("/old-students/{subjectName}/{numberYear}")
    public ResponseEntity<Object> findStudentBySubjectsNameAndNumberYear(@PathVariable String subjectName,@PathVariable Integer numberYear) {
        try {
            return ResponseEntity.ok(mapper.toDtos(((StudentService) service).findStudentBySubjectsNameAndNumberYear(subjectName, numberYear)));
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/subject/{studentId}/{subjectId}")
    public ResponseEntity<String> addSubject(@PathVariable String studentId, @PathVariable Long subjectId) {
        try {
            return ResponseEntity.ok(((StudentService) service).addSubject(studentId, subjectId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/subject/{studentId}/{subjectId}")
    public ResponseEntity<String> removeSubject(@PathVariable String studentId, @PathVariable Long subjectId) {
        try {
            return ResponseEntity.ok(((StudentService) service).removeSubject(studentId, subjectId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
