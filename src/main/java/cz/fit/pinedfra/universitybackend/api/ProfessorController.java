package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.api.dto.ProfessorDto;
import cz.fit.pinedfra.universitybackend.business.EntityStateException;
import cz.fit.pinedfra.universitybackend.business.ProfessorService;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.ProfessorMapper;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/professor")
@CrossOrigin
public class ProfessorController extends AbstractCrudController<Professor, ProfessorDto, String> {

    public ProfessorController(ProfessorService service, ProfessorMapper mapper) {
        super(service, mapper);
    }

    @GetMapping("/subject/{id}")
    public ResponseEntity<Object> findProfessorBySubjectsId(@PathVariable Long id) {
        try {
            return ResponseEntity.ok(mapper.toDtos(((ProfessorService) service).findProfessorBySubjectsId(id)));
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @PostMapping("/subject/{professorId}/{subjectId}")
    public ResponseEntity<String> addSubject(@PathVariable String professorId, @PathVariable Long subjectId) {
        try {
            var result = ((ProfessorService) service).addSubject(professorId, subjectId);
            return ResponseEntity.ok(result);
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }

    @DeleteMapping("/subject/{professorId}/{subjectId}")
    public ResponseEntity<String> removeSubject(@PathVariable String professorId, @PathVariable Long subjectId) {
        try {
            return ResponseEntity.ok(((ProfessorService) service).removeSubject(professorId, subjectId));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }
    }
}
