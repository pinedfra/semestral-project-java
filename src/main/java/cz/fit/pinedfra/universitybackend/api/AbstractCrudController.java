package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.api.dto.mapper.CrudMapper;
import cz.fit.pinedfra.universitybackend.business.AbstractCrudService;
import cz.fit.pinedfra.universitybackend.business.EntityStateException;
import cz.fit.pinedfra.universitybackend.domain.DomainEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.Collection;
import java.util.Optional;

public class AbstractCrudController<E extends DomainEntity<K>, D, K> {

    protected AbstractCrudService<E, K> service;

    protected CrudMapper<E, D> mapper;

    public AbstractCrudController(AbstractCrudService<E, K> service, CrudMapper<E, D> mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @PostMapping
    public ResponseEntity<Object> create(@RequestBody D dto) {
        try {
            E entity = mapper.toEntity(dto);
            E created = service.create(entity);
            return ResponseEntity.status(HttpStatus.CREATED).body(mapper.toDto(created));
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<Object> readById(@PathVariable("id") K id) {
        Optional<E> entity = service.readById(id);
        return entity.<ResponseEntity<Object>>map(e -> ResponseEntity.ok(mapper.toDto(e))).orElseGet(() -> ResponseEntity.status(HttpStatus.NOT_FOUND).body("{\"message\":\"Entity not found\"}"));
    }

    @GetMapping
    public Iterable<D> readAll() {
        return mapper.toDtos((Collection<E>) service.readAll());
    }

    @PutMapping()
    public ResponseEntity<Object> update(@RequestBody D dto) {
        try {
            E entity = mapper.toEntity(dto);
            E updated = service.update(entity);
            return ResponseEntity.status(HttpStatus.OK).body(mapper.toDto(updated));
        } catch (EntityStateException e) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") K id) {
        if (service.deleteById(id)) {
            return ResponseEntity.noContent().build();
        } else {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body("Entity not found");
        }
    }
}
