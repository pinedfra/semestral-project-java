package cz.fit.pinedfra.universitybackend.api.dto.mapper;

import cz.fit.pinedfra.universitybackend.api.dto.ProfessorDto;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProfessorMapper extends CrudMapper<Professor, ProfessorDto> {
}

