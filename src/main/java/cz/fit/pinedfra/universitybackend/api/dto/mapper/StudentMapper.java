package cz.fit.pinedfra.universitybackend.api.dto.mapper;

import cz.fit.pinedfra.universitybackend.api.dto.StudentDto;
import cz.fit.pinedfra.universitybackend.domain.Student;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StudentMapper extends CrudMapper<Student, StudentDto> {
}
