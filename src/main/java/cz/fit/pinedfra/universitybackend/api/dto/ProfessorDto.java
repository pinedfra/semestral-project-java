package cz.fit.pinedfra.universitybackend.api.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * A DTO for the {@link cz.fit.pinedfra.universitybackend.domain.Professor} entity
 */
@Getter
@Setter
public class ProfessorDto implements Serializable {
    private String id;
    private String firstName;
    private String lastName;
    private String facultyName;
    private String address = "";
    private String email = "";
    private String cellphone = "";
}