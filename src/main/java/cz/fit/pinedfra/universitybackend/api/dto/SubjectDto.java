package cz.fit.pinedfra.universitybackend.api.dto;

import cz.fit.pinedfra.universitybackend.domain.Subject;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.List;

/**
 * A DTO for the {@link Subject} entity
 */
@Getter
@Setter
public class SubjectDto implements Serializable {
    private long id;
    private String name;
    private int credits;
}