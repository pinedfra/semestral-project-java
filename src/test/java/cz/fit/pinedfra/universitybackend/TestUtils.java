package cz.fit.pinedfra.universitybackend;

import cz.fit.pinedfra.universitybackend.api.dto.ProfessorDto;
import cz.fit.pinedfra.universitybackend.api.dto.StudentDto;
import cz.fit.pinedfra.universitybackend.api.dto.SubjectDto;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.domain.Student;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

public class TestUtils {

    public static ProfessorDto createProfessorDto(String id, String firstName, String lastName, String facultyName) {
        ProfessorDto dto = new ProfessorDto();
        dto.setId(id);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setFacultyName(facultyName);
        return dto;
    }

    public static Professor createProfessorBusiness(String id, String firstName, String lastName, String facultyName, Subject... subjects ) {
        Professor professor = new Professor();
        professor.setId(id);
        professor.setFirstName(firstName);
        professor.setLastName(lastName);
        professor.setFacultyName(facultyName);
        for (Subject subject : subjects) {
            professor.addSubject(subject);
        }
        return professor;
    }

    public static Professor createProfessor(TestEntityManager entityManager, String id, String firstName, String lastName, String facultyName, Subject... subjects) {
        Professor professor = new Professor();
        professor.setId(id);
        professor.setFirstName(firstName);
        professor.setLastName(lastName);
        professor.setFacultyName(facultyName);
        for (Subject subject : subjects) {
            professor.getSubjects().add(subject);
        }
        entityManager.persist(professor);
        return professor;
    }

    public static SubjectDto createSubjectDto(String name, int credits) {
        SubjectDto dto = new SubjectDto();
        dto.setName(name);
        dto.setCredits(credits);
        return dto;
    }

    public static Subject createSubjectBusiness(String name, int credits) {
        Subject subject = new Subject();
        subject.setName(name);
        subject.setCredits(credits);
        return subject;
    }

    public static Subject createSubject(TestEntityManager entityManager, String name, int credits) {
        Subject subject = new Subject();
        subject.setName(name);
        subject.setCredits(credits);
        entityManager.persist(subject);
        return subject;
    }

    public static StudentDto createStudentDto(String id, String firstName, String lastName, String facultyName, int numberYear) {
        StudentDto dto = new StudentDto();
        dto.setId(id);
        dto.setFirstName(firstName);
        dto.setLastName(lastName);
        dto.setFacultyName(facultyName);
        dto.setNumberYear(numberYear);
        return dto;
    }

    public static Student createStudentBusiness(String id, String firstName, String lastName,
                                        String facultyName, int numberYear, Subject... subjects) {
        Student student = new Student();
        student.setId(id);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setFacultyName(facultyName);
        student.setNumberYear(numberYear);
        for (Subject subject : subjects) {
            student.getSubjects().add(subject);
        }
        return student;
    }

    public static Student createStudent(TestEntityManager entityManager, String id, String firstName, String lastName,
                                  String facultyName, int numberYear, Subject... subjects) {
        Student student = new Student();
        student.setId(id);
        student.setFirstName(firstName);
        student.setLastName(lastName);
        student.setFacultyName(facultyName);
        student.setNumberYear(numberYear);
        for (Subject subject : subjects) {
            student.getSubjects().add(subject);
        }
        entityManager.persist(student);
        return student;
    }
}
