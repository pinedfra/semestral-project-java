package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.api.dto.StudentDto;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.StudentMapper;
import cz.fit.pinedfra.universitybackend.business.StudentService;
import cz.fit.pinedfra.universitybackend.domain.Student;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@WebMvcTest(StudentController.class)
public class StudentApiTest {
    
    @Autowired
    MockMvc mockMvc;
    
    @MockBean
    StudentService studentService;
    
    @MockBean
    StudentMapper studentMapper;

    @Test
    public void testCreate() throws Exception {
        StudentDto dto = TestUtils.createStudentDto("123","John","Doe", "FIT", 3);

        Student student = TestUtils.createStudentBusiness("123","John","Doe", "FIT", 3);

        Mockito.when(studentMapper.toEntity(dto)).thenReturn(student);
        Mockito.when(studentService.create(student)).thenReturn(student);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": \"123\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"facultyName\": \"FIT\", \"numberYear\": 3}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testReadAll() throws Exception {
        StudentDto dto1 = TestUtils.createStudentDto("123","John","Doe", "FIT", 3);
        StudentDto dto2 = TestUtils.createStudentDto("456","Jane","Doe", "FIT", 3);

        Student student1 = TestUtils.createStudentBusiness("123","John","Doe", "FIT", 3);
        Student student2 = TestUtils.createStudentBusiness("456","Jane","Doe", "FIT", 3);

        List<Student> students = List.of(student1, student2);
        List<StudentDto> dtos = List.of(dto1, dto2);

        Mockito.when(studentService.readAll()).thenReturn(students);
        Mockito.when(studentMapper.toDtos(students)).thenReturn(dtos);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/student")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    public void testReadById() throws Exception {
        StudentDto dto = TestUtils.createStudentDto("123","John","Doe", "FIT", 3);

        Student student = TestUtils.createStudentBusiness("123","John","Doe", "FIT", 3);

        Mockito.when(studentService.readById("123")).thenReturn(Optional.of(student));
        Mockito.when(studentMapper.toDto(student)).thenReturn(dto);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/student/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is("123")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", Matchers.is("John")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", Matchers.is("Doe")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.facultyName", Matchers.is("FIT")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.numberYear", Matchers.is(3)));
    }

    @Test
    public void testUpdate() throws Exception {
        StudentDto dto = TestUtils.createStudentDto("123","John","Doe", "FIT", 3);

        Student student = TestUtils.createStudentBusiness("123","John","Doe", "FIT", 3);

        Mockito.when(studentMapper.toEntity(dto)).thenReturn(student);
        Mockito.when(studentService.update(student)).thenReturn(student);

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": \"123\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"facultyName\": \"FIT\", \"numberYear\": 3}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDelete() throws Exception {
        Mockito.doReturn(true).when(studentService).deleteById("123");

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/student/123")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.doReturn(false).when(studentService).deleteById("123");

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/student/123")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }

    @Test
    public void testfindStudentBySubjectsNameAndNumberYear() throws Exception {
        StudentDto dto1 = TestUtils.createStudentDto("123","John","Doe", "FIT", 3);
        StudentDto dto2 = TestUtils.createStudentDto("456","Jane","Doe", "FIT", 3);

        Student student1 = TestUtils.createStudentBusiness("123","John","Doe", "FIT", 3);
        Student student2 = TestUtils.createStudentBusiness("456","Jane","Doe", "FIT", 3);

        List<Student> students = List.of(student1, student2);
        List<StudentDto> dtos = List.of(dto1, dto2);

        Mockito.when(studentService.findStudentBySubjectsNameAndNumberYear("Math", 3)).thenReturn(students);
        Mockito.when(studentMapper.toDtos(students)).thenReturn(dtos);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/student/old-students/Math/3")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)));
    }

}
