package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.api.dto.SubjectDto;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.SubjectMapper;
import cz.fit.pinedfra.universitybackend.business.SubjectService;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@WebMvcTest(SubjectController.class)
public class SubjectApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    SubjectService subjectService;

    @MockBean
    SubjectMapper subjectMapper;

    @Test
    public void testCreate() throws Exception {
        SubjectDto dto = TestUtils.createSubjectDto("TJV", 5);
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);

        Mockito.when(subjectMapper.toEntity(dto)).thenReturn(subject);
        Mockito.when(subjectService.create(subject)).thenReturn(subject);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/subject")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"TJV\", \"credits\": 5}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testReadAll() throws Exception {
        SubjectDto dto1 = TestUtils.createSubjectDto("TJV", 5);
        SubjectDto dto2 = TestUtils.createSubjectDto("OOP", 4);

        Subject subject1 = TestUtils.createSubjectBusiness("TJV", 5);
        Subject subject2 = TestUtils.createSubjectBusiness("OOP", 4);

        List<Subject> subjects = List.of(subject1, subject2);
        List<SubjectDto> dtos = List.of(dto1, dto2);

        Mockito.when(subjectService.readAll()).thenReturn(subjects);
        Mockito.when(subjectMapper.toDtos(subjects)).thenReturn(dtos);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/subject")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].name", Matchers.is("TJV")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].credits", Matchers.is(5)))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].name", Matchers.is("OOP")))
                .andExpect(MockMvcResultMatchers.jsonPath("$[1].credits", Matchers.is(4)));
    }

    @Test
    public void testReadById() throws Exception {
        SubjectDto dto = TestUtils.createSubjectDto("TJV", 5);
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);

        Mockito.when(subjectService.readById(1L)).thenReturn(Optional.of(subject));
        Mockito.when(subjectMapper.toDto(subject)).thenReturn(dto);

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/subject/1")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.name", Matchers.is("TJV")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.credits", Matchers.is(5)));
    }

    @Test
    public void testUpdate() throws Exception {
        SubjectDto dto = TestUtils.createSubjectDto("TJV", 5);
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);

        Mockito.when(subjectMapper.toEntity(dto)).thenReturn(subject);
        Mockito.when(subjectService.update(subject)).thenReturn(subject);

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/subject")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"name\": \"TJV\", \"credits\": 5}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDelete() throws Exception {
        Mockito.doReturn(true).when(subjectService).deleteById(1L);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/subject/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.doReturn(false).when(subjectService).deleteById(1L);

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/subject/1")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
