package cz.fit.pinedfra.universitybackend.api;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.api.dto.ProfessorDto;
import cz.fit.pinedfra.universitybackend.api.dto.mapper.ProfessorMapper;
import cz.fit.pinedfra.universitybackend.business.ProfessorService;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.List;
import java.util.Optional;

@ActiveProfiles("test")
@WebMvcTest(ProfessorController.class)
public class ProfessorApiTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ProfessorService professorService;

    @MockBean
    ProfessorMapper professorMapper;

    @Test
    public void testCreate() throws Exception {
        ProfessorDto dto = TestUtils.createProfessorDto("123","John","Doe", "FIT");

        Professor professor = TestUtils.createProfessorBusiness("123","John","Doe", "FIT");

        Mockito.when(professorMapper.toEntity(dto)).thenReturn(professor);
        Mockito.when(professorService.create(professor)).thenReturn(professor);

        mockMvc.perform(MockMvcRequestBuilders
                        .post("/professor")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": \"123\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"faculty\": \"FIT\"}"))
                .andExpect(MockMvcResultMatchers.status().isCreated());
    }

    @Test
    public void testReadAll() throws Exception {
        ProfessorDto dto1 = TestUtils.createProfessorDto("123","John","Doe", "FIT");
        ProfessorDto dto2 = TestUtils.createProfessorDto("456","Jane","Doe", "FIT");

        Professor professor1 = TestUtils.createProfessorBusiness("123","John","Doe", "FIT");
        Professor professor2 = TestUtils.createProfessorBusiness("456","Jane","Doe", "FIT");

        List<Professor> professors = List.of(professor1, professor2);
        List<ProfessorDto> dtos = List.of(dto1, dto2);

        Mockito.when(professorService.readAll()).thenReturn(professors);
        Mockito.when(professorMapper.toDtos(professors)).thenReturn(dtos);


        mockMvc.perform(MockMvcRequestBuilders
                        .get("/professor")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$", Matchers.hasSize(2)));
    }

    @Test
    public void testReadById() throws Exception {
        ProfessorDto dto = TestUtils.createProfessorDto("123","John","Doe", "FIT");

        Professor professor = TestUtils.createProfessorBusiness("123","John","Doe", "FIT");

        Mockito.when(professorMapper.toDto(professor)).thenReturn(dto);
        Mockito.when(professorService.readById("123")).thenReturn(Optional.of(professor));

        mockMvc.perform(MockMvcRequestBuilders
                        .get("/professor/123")
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", Matchers.is("123")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", Matchers.is("John")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", Matchers.is("Doe")))
                .andExpect(MockMvcResultMatchers.jsonPath("$.facultyName", Matchers.is("FIT")));
    }

    @Test
    public void testUpdate() throws Exception {
        ProfessorDto dto = TestUtils.createProfessorDto("123","John","Doe", "FIT");

        Professor professor = TestUtils.createProfessorBusiness("123","John","Doe", "FIT");

        Mockito.when(professorMapper.toEntity(dto)).thenReturn(professor);
        Mockito.when(professorService.update(professor)).thenReturn(professor);

        mockMvc.perform(MockMvcRequestBuilders
                        .put("/professor")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("{\"id\": \"123\", \"firstName\": \"John\", \"lastName\": \"Doe\", \"faculty\": \"FIT\"}"))
                .andExpect(MockMvcResultMatchers.status().isOk());
    }

    @Test
    public void testDelete() throws Exception {
        Mockito.doReturn(true).when(professorService).deleteById("123");

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/professor/123")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNoContent());

        Mockito.doReturn(false).when(professorService).deleteById("123");

        mockMvc.perform(MockMvcRequestBuilders
                        .delete("/professor/123")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isNotFound());
    }
}
