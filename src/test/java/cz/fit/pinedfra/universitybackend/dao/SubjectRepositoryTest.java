package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.domain.Student;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.*;

@ActiveProfiles("test")
@DataJpaTest
class SubjectRepositoryTest {

    @Autowired
    SubjectRepository subjectRepository;

    @Autowired
    TestEntityManager entityManager;

    @AfterEach
    void tearDown() {
        subjectRepository.deleteAll();
    }

    @Test
    void findSubjectByStudentsId() {
        Subject tjv = TestUtils.createSubject(entityManager,"TJV", 5);
        Subject oop = TestUtils.createSubject(entityManager,"OOP", 4);

        Student s1 = TestUtils.createStudent(entityManager, "123", "Petr", "Lindner", "FIT", 2, tjv, oop);
        Student s2 = TestUtils.createStudent(entityManager,"456", "Daniel", "Kolář", "FIT", 3, tjv);

        assertEquals(2, subjectRepository.findSubjectByStudentsId(s1.getId()).size());
        assertEquals(1, subjectRepository.findSubjectByStudentsId(s2.getId()).size());
    }

    @Test
    void findSubjectByProfessorsId() {
        Subject tjv = TestUtils.createSubject(entityManager,"TJV", 5);
        Subject oop = TestUtils.createSubject(entityManager,"OOP", 4);

        Professor p1 = TestUtils.createProfessor(entityManager, "123", "Petr", "Lindner", "FIT", tjv, oop);
        Professor p2 = TestUtils.createProfessor(entityManager,"456", "Daniel", "Kolář", "FIT", tjv);

        assertEquals(2, subjectRepository.findSubjectByProfessorsId(p1.getId()).size());
        assertEquals(1, subjectRepository.findSubjectByProfessorsId(p2.getId()).size());
    }

}