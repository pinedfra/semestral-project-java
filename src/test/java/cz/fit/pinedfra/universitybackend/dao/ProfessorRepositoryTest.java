package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import java.util.Collection;

@ActiveProfiles("test")
@DataJpaTest
public class ProfessorRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    ProfessorRepository professorRepository;

    @AfterEach
    void tearDown() {
        professorRepository.deleteAll();
    }

    @Test
    void findProfessorBySubjectsId() {
        Subject tjv = TestUtils.createSubject(entityManager,"TJV", 5);
        Subject oop = TestUtils.createSubject(entityManager,"OOP", 4);

        Professor p1 = TestUtils.createProfessor(entityManager, "123", "Petr", "Lindner", "FIT", tjv, oop);
        Professor p2 = TestUtils.createProfessor(entityManager,"456", "Daniel", "Kolář", "FIT", tjv);

        Collection<Professor> professorsTjv = professorRepository.findProfessorBySubjectsId(tjv.getId());
        Collection<Professor> professorsOop = professorRepository.findProfessorBySubjectsId(oop.getId());

        Assertions.assertThat(professorsTjv).extracting(Professor::getFirstName).containsOnly("Petr", "Daniel");
        Assertions.assertThat(professorsOop).extracting(Professor::getFirstName).containsOnly("Petr");
    }
}