package cz.fit.pinedfra.universitybackend.dao;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.domain.Student;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ActiveProfiles("test")
@DataJpaTest
class StudentRepositoryTest {

    @Autowired
    StudentRepository studentRepository;

    @Autowired
    TestEntityManager entityManager;

    @AfterEach
    void tearDown() {
        studentRepository.deleteAll();
    }

    @Test
    void findStudentBySubjectsNameAndNumberYear() {
        Subject tjv = TestUtils.createSubject(entityManager,"TJV", 5);
        Subject oop = TestUtils.createSubject(entityManager,"OOP", 4);

        Student s1 = TestUtils.createStudent(entityManager, "123", "Petr", "Lindner", "FIT", 2, tjv, oop);
        Student s2 = TestUtils.createStudent(entityManager,"456", "Daniel", "Kolář", "FIT", 3, tjv);

        assertEquals(2, studentRepository.findStudentBySubjectsNameAndNumberYear("TJV", 2).size());
        assertEquals(0, studentRepository.findStudentBySubjectsNameAndNumberYear("TJV", 4).size());
        assertEquals(1, studentRepository.findStudentBySubjectsNameAndNumberYear("OOP", 1).size());
        assertEquals(0, studentRepository.findStudentBySubjectsNameAndNumberYear("OOP", 3).size());
    }

    @Test
    void findStudentBySubjectsId() {
        Subject tjv = TestUtils.createSubject(entityManager,"TJV", 5);
        Subject oop = TestUtils.createSubject(entityManager,"OOP", 4);

        Student s1 = TestUtils.createStudent(entityManager, "123", "Petr", "Lindner", "FIT", 2, tjv, oop);
        Student s2 = TestUtils.createStudent(entityManager,"456", "Daniel", "Kolář", "FIT", 3, tjv);

        assertEquals(2, studentRepository.findStudentBySubjectsId(tjv.getId()).size());
        assertEquals(1, studentRepository.findStudentBySubjectsId(oop.getId()).size());
    }

}