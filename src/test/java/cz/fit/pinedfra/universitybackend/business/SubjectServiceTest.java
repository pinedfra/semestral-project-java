package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.dao.SubjectRepository;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class SubjectServiceTest {

    @Autowired
    SubjectService subjectService;

    @MockBean
    SubjectRepository subjectRepository;

    @AfterEach
    void tearDown() {
        reset(subjectRepository);
    }

    @Test
    void getAllSubjects() {
        //Given
        List<Subject> subjects = Arrays.asList(
                TestUtils.createSubjectBusiness("TJV", 5),
                TestUtils.createSubjectBusiness("OOP", 4),
                TestUtils.createSubjectBusiness("IDO", 3)
        );
        when(subjectRepository.findAll()).thenReturn(subjects);

        //When
        List<Subject> result = (List<Subject>) subjectService.readAll();

        //Then
        Assertions.assertEquals(subjects, result);
        verify(subjectRepository).findAll();
    }

    @Test
    void getSubjectById() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.findById(subject.getId())).thenReturn(Optional.of(subject));

        //When
        Optional<Subject> result = subjectService.readById(subject.getId());

        //Then
        Assertions.assertEquals(Optional.of(subject), result);
        verify(subjectRepository).findById(subject.getId());
    }

    @Test
    void getSubjectByWrongID() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.findById(subject.getId())).thenReturn(Optional.empty());

        //When
        Optional<Subject> result = subjectService.readById(subject.getId());

        //Then
        Assertions.assertEquals(Optional.empty(), result);
        verify(subjectRepository).findById(subject.getId());
    }

    @Test
    void createSubject() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.save(subject)).thenReturn(subject);

        //When
        Subject result = subjectService.create(subject);

        //Then
        Assertions.assertEquals(subject, result);
        verify(subjectRepository).save(subject);
    }

    @Test
    void updateSubject() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        subjectRepository.save(subject);
        Subject updatedSubject = TestUtils.createSubjectBusiness("TJV", 7);
        when(subjectRepository.existsById(subject.getId())).thenReturn(true);
        when(subjectRepository.save(updatedSubject)).thenReturn(updatedSubject);

        //When
        Subject result = subjectService.update(updatedSubject);

        //Then
        Assertions.assertEquals(updatedSubject, result);
    }

    @Test
    void deleteSubject() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.existsById(subject.getId())).thenReturn(true);

        //When
        boolean result = subjectService.deleteById(subject.getId());

        //Then
        Assertions.assertTrue(result);
        verify(subjectRepository).existsById(subject.getId());
        verify(subjectRepository).deleteById(subject.getId());
    }

    @Test
    void deleteSubjectWithWrongID() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.existsById(subject.getId())).thenReturn(false);

        //When
        boolean result = subjectService.deleteById(subject.getId());

        //Then
        Assertions.assertFalse(result);
        verify(subjectRepository).existsById(subject.getId());
        verify(subjectRepository, never()).deleteById(subject.getId());
    }

    @Test
    void deleteSubjectWithNullID() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.existsById(subject.getId())).thenReturn(false);

        //When
        boolean result = subjectService.deleteById(null);

        //Then
        Assertions.assertFalse(result);
        verify(subjectRepository).existsById(null);
    }

    @Test
    void createSubjectWithIdAlreadyExisting() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.existsById(subject.getId())).thenReturn(true);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> subjectService.create(subject));

        //Then
        verify(subjectRepository, never()).save(subject);
    }

    @Test
    void updateSubjectWithIdNotExisting() {
        //Given
        Subject subject = TestUtils.createSubjectBusiness("TJV", 5);
        when(subjectRepository.existsById(subject.getId())).thenReturn(false);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> subjectService.update(subject));

        //Then
        verify(subjectRepository, never()).save(subject);
    }
}
