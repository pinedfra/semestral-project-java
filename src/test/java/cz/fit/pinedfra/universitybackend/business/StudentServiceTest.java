package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.dao.StudentRepository;
import cz.fit.pinedfra.universitybackend.domain.Student;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
public class StudentServiceTest {

    @Autowired
    StudentService studentService;

    @MockBean
    StudentRepository studentRepository;

    @AfterEach
    void tearDown() {
        reset(studentRepository);
    }

    @Test
    void getAllStudents() {
        //Given
        List<Student> students = Arrays.asList(
                TestUtils.createStudentBusiness("123", "Petr", "Petrov", "FIT", '2'),
                TestUtils.createStudentBusiness("456", "Jan", "Novak", "FIT", '3'),
                TestUtils.createStudentBusiness("789", "Pavel", "Novak", "FIT", '3')
        );
        when(studentRepository.findAll()).thenReturn(students);

        //When
        List<Student> result = (List<Student>) studentService.readAll();

        //Then
        Assertions.assertEquals(students, result);
        verify(studentRepository).findAll();
    }

    @Test
    void getStudentById() {
        //Given
        Student student = TestUtils.createStudentBusiness("123", "Petr", "Petrov", "FIT", '2');
        when(studentRepository.findById("123")).thenReturn(Optional.of(student));

        //When
        Optional<Student> result = studentService.readById("123");

        //Then
        Assertions.assertEquals(student, result.get());
        verify(studentRepository).findById("123");
    }

    @Test
    void getStudentByWrongId() {
        //Given
        when(studentRepository.findById("123")).thenReturn(Optional.empty());

        //When
        Optional<Student> result = studentService.readById("123");

        //Then
        Assertions.assertFalse(result.isPresent());
        verify(studentRepository).findById("123");
    }

    @Test
    void createStudent() {
        //Given
        Student student = TestUtils.createStudentBusiness("123", "Petr", "Petrov", "FIT", '2');
        when(studentRepository.save(student)).thenReturn(student);

        //When
        Student result = studentService.create(student);

        //Then
        Assertions.assertEquals(student, result);
        verify(studentRepository).save(student);
    }

    @Test
    void createStudentWithEmptyId() {
        //Given
        Student student = TestUtils.createStudentBusiness("", "John", "Doe", "FIT", '2');
        when(studentRepository.existsById("")).thenReturn(false);

        try {
            //When
            studentService.create(student);
            Assertions.fail("Exception should be thrown");
        } catch (EntityStateException e) {
            //Then
            Assertions.assertEquals("Invalid input", e.getMessage());
        }
    }

    @Test
    void createStudentWithIdAlreadyExisting() {
        //Given
        Student student = TestUtils.createStudentBusiness("123", "John", "Doe", "FIT", '2');
        when(studentRepository.existsById("123")).thenReturn(true);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> studentService.create(student));

        //Then
        verify(studentRepository).existsById("123");
    }

    @Test
    void updateStudent() {
        //Given
        Student student = TestUtils.createStudentBusiness("123", "Petr", "Petrov", "FIT", '2');
        studentRepository.save(student);
        Student studentUpdated = TestUtils.createStudentBusiness("123", "Petr", "Novak", "FIT", '2');
        when(studentRepository.existsById("123")).thenReturn(true);
        when(studentRepository.save(studentUpdated)).thenReturn(studentUpdated);

        //When
        Student result = studentService.update(studentUpdated);

        //Then
        Assertions.assertEquals(studentUpdated, result);
    }

    @Test
    void updateStudentWithNotExistingId() {
        //Given
        Student student = TestUtils.createStudentBusiness("123", "Petr", "Petrov", "FIT", '2');
        when(studentRepository.existsById("123")).thenReturn(false);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> studentService.update(student));

        //Then
        verify(studentRepository).existsById("123");
    }
}
