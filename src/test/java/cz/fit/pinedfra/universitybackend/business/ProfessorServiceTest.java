package cz.fit.pinedfra.universitybackend.business;

import cz.fit.pinedfra.universitybackend.dao.ProfessorRepository;
import cz.fit.pinedfra.universitybackend.TestUtils;
import cz.fit.pinedfra.universitybackend.dao.SubjectRepository;
import cz.fit.pinedfra.universitybackend.domain.Professor;
import cz.fit.pinedfra.universitybackend.domain.Subject;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.*;

@ActiveProfiles("test")
@SpringBootTest
class ProfessorServiceTest {

    @Autowired
    ProfessorService professorService;

    @MockBean
    ProfessorRepository professorRepository;

    @MockBean
    SubjectRepository subjectRepository;

    @AfterEach
    void tearDown() {
        reset(professorRepository);
        reset(subjectRepository);
    }

    @Test
    void getAllProfessors() {
        //Given
        List<Professor> professors = Arrays.asList(
                TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT"),
                TestUtils.createProfessorBusiness("456", "Jan", "Novak", "FIT"),
                TestUtils.createProfessorBusiness("789", "Pavel", "Novak", "FIT")
        );
        when(professorRepository.findAll()).thenReturn(professors);

        //When
        List<Professor> result = (List<Professor>) professorService.readAll();

        //Then
        Assertions.assertEquals(professors, result);
        verify(professorRepository).findAll();
    }

    @Test
    void getProfessorById() {
        //Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        when(professorRepository.findById("123")).thenReturn(java.util.Optional.of(professor));

        //When
        Optional<Professor> result = professorService.readById("123");

        //Then
        Assertions.assertEquals(professor, result.get());
        verify(professorRepository).findById("123");
    }

    @Test
    void getProfessorByWrongId() {
        //Given
        when(professorRepository.findById("123")).thenReturn(Optional.empty());

        //When
        Optional<Professor> result = professorService.readById("123");

        //Then
        Assertions.assertFalse(result.isPresent());
        verify(professorRepository).findById("123");
    }

    @Test
    void createProfessor() {
        //Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        when(professorRepository.save(professor)).thenReturn(professor);

        //When
        Professor result = professorService.create(professor);

        //Then
        Assertions.assertEquals(professor, result);
        verify(professorRepository).save(professor);
    }

    @Test
    void updateProfessor() {
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        professorRepository.save(professor);
        Professor professorUpdate = TestUtils.createProfessorBusiness("123", "Petr", "Novak", "FIT");
        when(professorRepository.existsById("123")).thenReturn(true);
        when(professorRepository.save(professorUpdate)).thenReturn(professorUpdate);

        //When
        Professor result = professorService.update(professorUpdate);

        //Then
        Assertions.assertEquals(professorUpdate, result);
    }

    @Test
    void deleteProfessor() {
        //Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        when(professorRepository.existsById("123")).thenReturn(true);

        //When
        boolean resutl = professorService.deleteById("123");

        //Then
        Assertions.assertTrue(resutl);
        verify(professorRepository).existsById("123");
        verify(professorRepository).deleteById("123");
    }

    @Test
    void deleteProfessorWithWrongId() {
        //Given
        when(professorRepository.existsById("123")).thenReturn(false);

        //When
        boolean resutl = professorService.deleteById("123");

        //Then
        Assertions.assertFalse(resutl);
        verify(professorRepository).existsById("123");
    }

    @Test
    void deleteProfessorWithNullId() {
        //Given
        when(professorRepository.existsById(null)).thenReturn(false);

        //When
        boolean resutl = professorService.deleteById(null);

        //Then
        Assertions.assertFalse(resutl);
        verify(professorRepository).existsById(null);
    }

    @Test
    void createProfessorWithIdAlreadyExisting() {
        //Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        when(professorRepository.existsById("123")).thenReturn(true);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> professorService.create(professor));

        //Then
        verify(professorRepository).existsById("123");
    }

    @Test
    void createProfessorWithEmptyId() {
        Professor professor = TestUtils.createProfessorBusiness("", "Petr", "Petrov", "FIT");
        when(professorRepository.existsById("")).thenReturn(false);

        try {
            //When
            professorService.create(professor);
            Assertions.fail("Exception should be thrown");
        } catch (EntityStateException e) {
            //Then
            Assertions.assertEquals("Invalid input", e.getMessage());
        }
    }

    @Test
    void updateProfessorWithIdNotExisting() {
        //Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        when(professorRepository.existsById("123")).thenReturn(false);

        //When
        Assertions.assertThrows(EntityStateException.class, () -> professorService.update(professor));

        //Then
        verify(professorRepository).existsById("123");
    }

    @Test
    void addSubject() {
        // Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        Subject subject = TestUtils.createSubjectBusiness("Math", 5);
        when(professorRepository.findById("123")).thenReturn(Optional.of(professor));
        when(subjectRepository.findById(1L)).thenReturn(Optional.of(subject));

        // When
        String result = professorService.addSubject("123", 1L);

        // Then
        Assertions.assertEquals("Subject added", result);
        verify(professorRepository).findById("123");
        verify(subjectRepository).findById(1L);
        verify(professorRepository).save(professor);
    }

    @Test
    void removeSubject() {
        // Given
        Professor professor = TestUtils.createProfessorBusiness("123", "Petr", "Petrov", "FIT");
        Subject subject = TestUtils.createSubjectBusiness("Math", 5);

        when(professorRepository.findById("123")).thenReturn(Optional.of(professor));
        when(subjectRepository.findById(1L)).thenReturn(Optional.of(subject));

        // When
        String result = professorService.removeSubject("123", 1L);

        // Then
        Assertions.assertEquals("The professor is not teaching this subject", result);
        verify(professorRepository).findById("123");
        verify(subjectRepository).findById(1L);
    }

}